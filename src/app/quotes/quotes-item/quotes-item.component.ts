import { Component, OnInit } from '@angular/core';
import { Quote } from '../../shared/quote.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-quotes-item',
  templateUrl: './quotes-item.component.html',
  styleUrls: ['./quotes-item.component.css']
})
export class QuotesItemComponent implements OnInit {
  quotes!: Quote[];
  quote!: Quote;
  id!: string;


  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.quotes = [];
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.getQuotes();
    })
  }

  getQuotes() {
    let link = '';
    if(this.id === 'All') {
      link = 'https://plovo-e531e-default-rtdb.firebaseio.com/quotes.json';
    } else {
      link = `https://plovo-e531e-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="${this.id}"`;
      console.log(this.id)
    }
    this.http.get<{[id: string]: Quote}>(link)
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const quoteData = result[id];
          return new Quote(id, quoteData.author, quoteData.text, quoteData.category);
        });
      }))
      .subscribe(quotes => {
        this.quotes = quotes;
      });
  }


  delete(id: string) {
    this.http.delete<Quote>(`https://plovo-e531e-default-rtdb.firebaseio.com/quotes/${id}.json`).subscribe();
    void this.router.navigate(['/quotes']);
  }
}
