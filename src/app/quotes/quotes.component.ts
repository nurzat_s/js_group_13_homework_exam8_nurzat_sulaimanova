import { Component, OnInit } from '@angular/core';
import { Quote } from '../shared/quote.model';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  quotes!: Quote[];
  category = ['All', 'Star Wars', 'Famous people', 'Saying', 'Humour', 'Motivational'];
  constructor() { }

  ngOnInit(): void {}
}
