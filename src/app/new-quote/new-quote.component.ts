import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quote } from '../shared/quote.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {
  category!: string;
  author!: string;
  text!: string;
  id!: string;

  constructor(private http: HttpClient,  private router: Router, private route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      this.id = id;
      this.http.get<Quote>(`https://plovo-e531e-default-rtdb.firebaseio.com/quotes/${id}.json`)
        .pipe(map(quote => {
          return new Quote(quote.id, quote.author, quote.category, quote.text);
        }))
        .subscribe(quote => {
          this.category = quote.category;
          this.author = quote.author;
          this.text = quote.text;
        });
    });
  }

  createQuote() {
    const body = {category: this.category, author: this.author, text: this.text};
    if(this.id) {
      this.http.put(`https://plovo-e531e-default-rtdb.firebaseio.com/quotes/${this.id}.json`, body).subscribe();
    }
    else {
      this.http.post('https://plovo-e531e-default-rtdb.firebaseio.com/quotes.json', body).subscribe();
    }
    this.http.get<{[id: string]: Quote}>('https://plovo-e531e-default-rtdb.firebaseio.com/quotes.json').subscribe();
    void this.router.navigate(['/']);
    console.log(this.http)
  }


}
