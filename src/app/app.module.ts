import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { QuotesComponent } from './quotes/quotes.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { QuotesItemComponent } from './quotes/quotes-item/quotes-item.component';

@NgModule({
  declarations: [
    AppComponent,
    QuotesComponent,
    ToolbarComponent,
    NewQuoteComponent,
    QuotesItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
