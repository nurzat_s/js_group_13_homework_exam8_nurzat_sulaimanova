import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuotesComponent } from './quotes/quotes.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { QuotesItemComponent } from './quotes/quotes-item/quotes-item.component';

const routes: Routes = [
  {path: '', component: QuotesComponent},
  {path: 'quotes', component: QuotesComponent, children: [
      {path: ':id', component: QuotesItemComponent}
    ]},
  {path: 'edit', component: NewQuoteComponent},
  {path: 'new', component: NewQuoteComponent},

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
